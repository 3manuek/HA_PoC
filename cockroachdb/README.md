## Get in

```
docker-compose exec roach-one ./cockroach sql --insecure
```

From your browser: http://localhost:18080


Using autoscale:

```
docker-compose -f docker-compose-autoscale.yml up --scale roach-ui=1 --scale roach-node=3
docker-compose -f docker-compose-autoscale.yml down
```

## Cert


Fixing certs should be easy as [this](https://github.com/cockroachdb/cockroach/issues/22537#issuecomment-364425557):

```
mkdir server-certs client-certs

./cockroach cert create-ca --certs-dir=server-certs --ca-key=server-certs/ca.key

./cockroach cert create-node --certs-dir=server-certs --ca-key=server-certs/ca.key localhost 127.0.0.1
#$ ls server-certs/
#ca.crt  ca.key  client.root.crt  client.root.key  node.crt  node.key

# Start the node:
$ cockroach start --certs-dir=server-certs
```

You need to replace `--insecure` to `--certs-dir=server-certs`.

## RAFT extended

Multi-RAFT was announced [here](https://www.cockroachlabs.com/blog/scaling-raft/).
Each node may be participating in hundreds of thousands of consensus groups, causing
too many messages between groups when using single-message based RAFT.

Instead of allowing each range to run Raft independently, the entire node’s worth of ranges is manged as a group. 
Each pair of nodes only needs to exchange heartbeats once per tick, no matter how many ranges they have in common.

Does this adds a delay on top of consensus? Does this somehow forces the serialization of the RAFT messages tied to 
a constraint? Which is the threshold on top of the number of groups needed to see multi-RAFT performing better than plain RAFT?