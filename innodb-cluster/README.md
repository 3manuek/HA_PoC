## Warning

Consul component is still in dev. Concept is to install a cluster (3 nodes) and install 
the consul agent on mysql-router, to point to the available masters. This may not be easy,
as it is planned to use more than one Group Replication, which may end in more than 1 endpoint.
At that state, we may need to have more than a single router.



## Usage

How to connect :



```bash
docker-compose exec mysql-server-1 bash
bash-4.2# mysql -h mysql-router -P 6446  -u root -p 
```

> mysql is the password


https://dev.mysql.com/doc/refman/5.7/en/mysql-innodb-cluster-production-deployment.html
https://mysqlrelease.com/2018/03/docker-compose-setup-for-innodb-cluster/


Another source and examples:
https://github.com/neumayer/mysql-docker-compose-examples.git


Offical images:
https://github.com/mysql/mysql-docker


Consul HA related links:

https://lefred.be/content/mysql-ha-architecture-1-innodb-cluster-consul/
https://blog.pythian.com/mysql-high-availability-with-haproxy-consul-and-orchestrator/



Group Replication links and recommended reads: 
https://www.percona.com/blog/2018/07/09/innodb-cluster-in-a-nutshell-part-1/
https://thesubtlepath.com/blog/mysql/understanding-group-replication-and-innodb-cluster/
https://thesubtlepath.com/blog/mysql/group-replication-minimal-sql-based-configuration/


## TODO

- Change scripts/db.sql to setup group replication first steps.
- install consul agent on routers and configure the failover watchers.