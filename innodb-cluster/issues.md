
## volume pinned by phantom container

```
mysql-server-1_1  | 2018-08-26T22:02:09.866307Z 0 [System] [MY-013169] [Server] /usr/sbin/mysqld (mysqld 8.0.12) initializing of server in progress as process 23
mysql-server-1_1  | 2018-08-26T22:02:09.868110Z 0 [ERROR] [MY-010457] [Server] --initialize specified but the data directory has files in it. Aborting.
mysql-server-1_1  | 2018-08-26T22:02:09.868158Z 0 [ERROR] [MY-010119] [Server] Aborting
```

fixed by removing images from innodb_cluster


## auth issue default in mysql 8:0

```
mysql-shell_1     | ERROR: 2059 (HY000): Authentication plugin 'caching_sha2_password' cannot be loaded: /usr/lib64/mysql/plugin/caching_sha2_password.so: cannot open shared object file: No such file or directory
```

```
,"default_authentication_plugin=mysql_native_password"
```