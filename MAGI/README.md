## MAGI

[MAGI](https://wiki.evageeks.org/Magi) is a set of Consul Servers to serve all the
consensus of the clusters of this lab.

```bash
docker-compose up --scale consul-bootstrap=1 --scale consul-server=2 #MAGI
```

## Network

Network needs to be used as [external in other composes](https://docs.docker.com/compose/networking/#use-a-pre-existing-network). 

```bash
docker network inspect magi_magi-net | jq '.[].Containers[] | select(.Name == "magi_consul-bootstrap_1") | .IPv4Address'
"192.168.0.3/20"
```

[Multiple bind address](https://github.com/hashicorp/consul/pull/3480)

## Applying custom conf

config directory isn't open in the official docker image, in order to do so,
the variable `CONSUL_LOCAL_CONFIG` must be used to pass through JSON code.