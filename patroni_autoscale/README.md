## Patroni Autoscale

```
docker-compose build
docker-compose up --scale consul-bootstrap=1 --scale consul-server=2 
docker-compose up --scale pg=3 --scale consul-agent=1
```

If using agent in `-dev` mode:

```
docker-compose up --scale pg=3 --scale consul-agent=1 --scale consul-bootstrap=0 --scale consul-server=0 --scale pgbouncer=0
```



Access the Consul UI through `http://localhost:18501`.

## Passing custom consul agent config

```
CONSUL_LOCAL_CONFIG="\
{\
  \"check\": {\
    \"id\": \"postgres\",\
    \"name\": \"postgres check\",\
    \"tcp\": \"pg-1:5432\",\
    \"interval\": \"10s\",\
    \"timeout\": \"1s\"\
  }\
}"

docker-compose up --scale consul-bootstrap=1 --scale consul-server=2 --scale pg=1 --scale pgbouncer=1

curl --request GET  http://localhost:18501/v1/agent/checks
{"postgres":{"Node":"a831115f0bf0","CheckID":"postgres","Name":"postgres check","Status":"passing","Notes":"","Output":"TCP connect 192.168.112.7:5432: Success","ServiceID":"","ServiceName":"","ServiceTags":[],"Definition":{},"CreateIndex":0,"ModifyIndex":0}}%

```