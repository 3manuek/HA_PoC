
## ProxySQL

Instead of MySQL router, ProxySQL is an alternative option with more features and flexibility.

Concept example:

https://lefred.be/content/mysql-group-replication-native-support-in-proxysql/

http://dasini.net/blog/2018/01/09/setting-up-proxysql-1-4-with-mysql-5-7-group-replication/



Some repos:

https://github.com/mszel42/Ansible-MHA-ProxySQL-Docker
https://github.com/pondix/docker-mysql-proxysql

HA is not covered through consensus. This repo intends to implement using Consul. 
Also, as there are no official images of proxysql, the Dockerfile will be included.


