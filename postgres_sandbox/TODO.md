## TODO

- Create packages for different purposes (dev, support) `vagrant package --output <department>ongres.box`
- Add pgbouncer, backup tools,etc. 
- Document how to apply only specific roles at Vagrant provisioning.
- We can build specific versions into docker containers. (Long task, although devilish `†_(~.~)/`  )
- pgc is great for quick test, although it does not support slave creation nor neither download specific versions. Maybe moving out but keeping the same structure might help (it is easily extensible, not sure about the copyright thing). 
- Enable sar automagically
- Add more disk and performance tools (like flamegraph also)
- Automate slave creation
- A reference of new tools for Postgres ecosystem can be found at https://wiki.postgresql.org/images/6/6a/Dba_toolbelt_2017.pdf
- A plain playbook for installing Postgres in any version using standard playbooks instead of pgc, allowing a number of slaves instead 
  of using custom scripts for spinning just one.
- Mount different devices in several file systems (zfs, betterfs, ext4, mdadm + lvm)