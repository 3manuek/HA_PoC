## pgtest vagrant/docker sandbox

It installs a Postgres environment.

### Setup env

```bash
vagrant plugin install vagrant-disksize
vagrant up
```


### How to custom build

The Vagrantfile checks certain environment variables on top of which you can customize which playbooks
apply at provision, choose/skip specific tasks or start from specific tasks:

```ruby
    ansible.playbook = ENV ['VAGRANT_PLAYBOOK'] || "provisioning/vagrant_playbook.yml" # Default
    ansible.sudo = true
    #ansible.limit = "all"
    ansible.tags = ENV['VAGRANT_TAGS']
    ansible.skip_tags = ENV['VAGRANT_SKIP_TAGS']
    ansible.extra_vars = JSON.load(ENV['VAGRANT_EXTRA_VARS'])
    ansible.start_at_task = ENV['VAGRANT_START_AT_TASK']
```

Export any of the variables and issue `vagrant provision`:

```
VAGRANT_START_AT_TASK="docker : Ensure old versions of Docker are not installed." vagrant provision
```



### Init an instance

```bash
pgc init pg95
pgc start pg95
pgc status
pgenv pg95 # For loading the environment
psql
```

[pgc documentation](https://www.openscg.com/bigsql/docs/pgcli/pgcli/). See `bigsql/hub/scripts` for internals (python).

PGXS are integrated in bigsql, so before compile any tool ensure that you loadad the corresponding environment.

## Setting a slave manually (pgc does not support this)

> WARNING: this has been already automated through `pgcslave <component>` for setting at least **1 slave**.

> NOTE: source the env before executing the commmands and change the component accordingly

Add into `pg_hba.conf` in data/pgxx directory and `pgc reload pgxx`:

```bash
cd $HOME/bigsql

cat <<lines >> data/pg96/pg_hba.conf
local   replication     all                                     trust
host    replication     all             0.0.0.0/0               trust
lines

pgc reload pg96


pg_basebackup -x -R -D data/pg96_slave
sed  -i 's/port = \([0-9]\+\)/port = 1\1/' data/pg96_slave/postgresql.conf # adds 1 on top of the port
sed -i 's/wal_level = \([a-z|_]\+\)/wal_level = logical/' data/pg96/postgresql.conf   # not mandatory but we'll get used to play with logical soon
echo "## Added manually" >> data/pg96_slave/postgresql.conf
echo "hot_standby = on" >> data/pg96_slave/postgresql.conf

# Adding pgpass entries
# sed  -n 's/port[[:space:]]\+=[[:space:]]\+\([0-9]\+\)[[:space:]]\+\(#.*\)/\1/p' pg96/postgresql.conf
masterPort=$(sed  -n 's/port[[:space:]]\+=[[:space:]]\+\([0-9]\+\)[[:space:]]\+\(#.*\)/\1/p' data/pg96/postgresql.conf) 
grep $masterPort $PGPASSFILE | sed "s/\(${masterPort}\)/1\1/" >> $PGPASSFILE

pg_ctl start -D data/pg96_slave/
psql -p 1${masterPort}
```


## Organization 

- Ansibilize as agnostic as possible so we can mirror this into an EC2 or any other centralized vm.
- You can add as many roles as you want, but only add it in the playbook (vagrant_playbook.yml) once they are consistent and not failing.  
  Also we may want to have different playbooks depending on the machine we want to build (dev, support, RCA, etc)

## Generate image

```
vagrant package --output suppongres.box
```


## Highlights

- Postgres does not run in the machine using repository installation, instead go to the PostgreSQL directory at vagrant's home and manage instances through `pgc` command.
- We'll support atm only ubuntu, but we shuold make this agnostic as possible from the playbook perspective.


## Sources

Docker roles has been taken from [geerlingguy](https://github.com/geerlingguy/ansible-role-docker)


## Others

https://mermaidjs.github.io/ 

```mermaid
graph TD;
    X[vagrant_playbook.yml] --> A{provisioning};
    A --> C[postgres]
    A --> D[resources]
    A --> E[docker]
    A --> F[...]
    B[vars];
```
