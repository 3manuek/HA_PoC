#!/bin/bash

DOCKER_IP=$(hostname --ip-address)
PGBOUNCER_CONFIG=${PGBOUNCER_CUSTOM_CONFIG:-$1}
DEFAULT_CONSUL_ARGS="agent -advertise $DOCKER_IP -retry-join \"${CONSUL_BOOTSTRAP_HOST}\" -ui -client=0.0.0.0 -bind=0.0.0.0 -datacenter=${CONSUL_DATACENTER}"
CONSUL_ARGS=${CONSUL_CUSTOM_ARGS:-DEFAULT_CONSUL_ARGS}

# NAMESPACE is the namespace in consul, default is to service mostly always,
# even if several clusters are set
# CONFD="confd -prefix=${PATRONI_NAMESPACE:-/service}/$PATRONI_SCOPE -interval=10 -backend"


# gosu $PGBUSER consul ${CONSUL_ARGS} 2> /var/log/consul/consul.log &
consul $CONSUL_ARGS 2> /var/log/consul/consul.log &

export CONSUL_URL="http://127.0.0.1:8500"

# Disabled confd until we code the pgbouncer side of the watcher
# gosu $PGBUSER $CONFD consul -node $CONSUL_URL 2 > /var/log/confd/confd.log &

# exec gosu $PGBUSER pgbouncer ${PGBOUNCER_CONFIG}

exec pgbouncer  ${PGBOUNCER_CONFIG}

# while true; do
#     sleep 60
# done
