## Custom Images

Building some images that have no official release.

All related images are being worked https://gitlab.com/3manuek/docker_images



## PgBouncer container

Image created for the purpose of automate compilation of several versions. 

We should have an image to get from master and test


## Security 

This container only runs in pgbouncer, every change and installation should be done at
building time.

## Testing

There should be a consul agent running and the pgbouncer:

```
for ps in /proc/[0-9]*/cmdline; do cat $ps ; echo ; done
pgbouncer/etc/pgbouncer/pgbouncer.ini
bash
consulagent-dev
```


## Clean eveything

```bash
docker container ls --all |  grep -v CONTAINER | awk '{print $1}'  | xargs docker container rm
docker volume ls | grep -v VOLUME | awk ' {print$2}'  | xargs docker volume inspect | jq '.[].Name' | xargs docker volume rm
```


Removing images:

```
docker images "docker*" -q | xargs docker rmi
docker images | grep "<none>" | awk '{print $3}' | xargs docker rmi
```
