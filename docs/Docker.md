
## Persistent Volumes over Stack

[Documentation](https://docs.docker.com/compose/compose-file/#volumes-for-services-swarms-and-stack-files)

> VOLUMES FOR SERVICES, SWARMS, AND STACK FILES

When working with services, swarms, and docker-stack.yml files, keep in mind that the tasks (containers) backing a service can be deployed on any node in a swarm, and this may be a different node each time the service is updated.

In the absence of having named volumes with specified sources, Docker creates an anonymous volume for each task backing a service. Anonymous volumes do not persist after the associated containers are removed.

If you want your data to persist, use a named volume and a volume driver that is multi-host aware, so that the data is accessible from any node. Or, set constraints on the service so that its tasks are deployed on a node that has the volume present.

As an example, the docker-stack.yml file for the votingapp sample in Docker Labs defines a service called db that runs a postgres database. It is configured as a named volume to persist the data on the swarm, and is constrained to run only on manager nodes. Here is the relevant snip-it from that file:


```yaml
version: "3"
services:
  db:
    image: postgres:9.4
    volumes:
      - db-data:/var/lib/postgresql/data
    networks:
      - backend
    deploy:
      placement:
        constraints: [node.role == manager]
```


