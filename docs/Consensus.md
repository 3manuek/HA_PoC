## Paxos

https://lamport.azurewebsites.net/pubs/paxos-simple.pdf


## RAFT

https://raft.github.io/raft.pdf


## Consul

https://medium.com/zendesk-engineering/making-docker-and-consul-get-along-5fceda1d52b9
https://lefred.be/content/mysql-ha-architecture-1-innodb-cluster-consul/


## Consul templates

https://github.com/hashicorp/consul-template
