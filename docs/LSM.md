## LSM on distributed HA

Log-Structured merge-tree. Uses multiple levels and only C_0 is on memory. The only
way to make it consistent is by WAL.

## Cassandra

https://web.archive.org/web/20140213134601/http://www.datastax.com/dev/blog/leveled-compaction-in-apache-cassandra

## RocksDB

Although its nature is key-value, there are implementations on top that manage a relational model from the 
end user perspective (MyRocks and CockRoachDB).

