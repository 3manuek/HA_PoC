#!/bin/bash

export DROP="DROP TABLE IF EXISTS tangerine;"
export INZ="insert into tangerine default values;"
export INZ_CUSTOM="insert into tangerine(i) VALUES (1000);"
export COUNT="select count(*) from tangerine"
export CREATE_source="
CREATE TABLE IF NOT EXISTS tangerine( \
    i bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,\
    f text default random()::text \
);"

## Destination can be different, is very useful for aggregations
## on the destination and updates over the row.
export CREATE_destination="
CREATE TABLE IF NOT EXISTS tangerine( \
    i bigint GENERATED BY DEFAULT AS IDENTITY PRIMARY KEY,\
    f text default random()::text, \
    e text \
);"
export CHECK_VALUES="SELECT i,f FROM tangerine"



function ppf {

for pp in  5 6
do
echo $pp --
psql -q -h localhost -p 1555${pp} -U postgres <<EOF
${@}
EOF

done
}


echo "pubs"
psql -h localhost -p 15555 -U postgres <<EOF
DROP PUBLICATION full_tangerine;
EOF


ppf "drop subscription s_tangerine;"

echo "create"
ppf $DROP
psql -h localhost -p 15555 -U postgres <<EOF
${CREATE_source}
EOF
psql -h localhost -p 15556 -U postgres <<EOF
${CREATE_destination}
EOF

psql -h localhost -p 15555 -U postgres <<EOF
${INZ}
EOF


echo "pubs"
psql -h localhost -p 15555 -U postgres <<EOF
CREATE PUBLICATION full_tangerine FOR TABLE tangerine;
EOF


psql -h localhost -p 15556 -U postgres <<EOF
CREATE SUBSCRIPTION s_tangerine
  CONNECTION 'host=postgres1  user=postgres' PUBLICATION full_tangerine WITH (copy_data =  true, enabled = true);
EOF

sleep 0.5 
ppf $COUNT
ppf $CHECK_VALUES
psql -h localhost -p 15555 -U postgres <<EOF
${INZ}
EOF
psql -h localhost -p 15555 -U postgres <<EOF
${INZ}
EOF
psql -h localhost -p 15555 -U postgres <<EOF
${INZ}
EOF

ppf $CHECK_VALUES

echo "let's see what destination does inserting def values on destination"
psql -h localhost -p 15556 -U postgres <<EOF
${INZ}
EOF

echo "... but at least you can insert the record manually if you want to failover"
echo "the destination (receive writes). Although for consistency we recommend not"
echo "write on destination tables."

echo "BUT FIRST, you need to _fix_ the memory of the hidden sequence in destination"
psql -h localhost -p 15556 -U postgres <<EOF
ALTER TABLE tangerine ALTER COLUMN i RESTART WITH 100;
EOF

psql -h localhost -p 15556 -U postgres <<EOF
${INZ_CUSTOM}
EOF

# What happens if you try to insert default keys? -- well, postgres
# iterates until it works. :)
# postgres=# insert into tangerine default values;
# ERROR:  duplicate key value violates unique constraint "tangerine_pkey"
# DETAIL:  Key (i)=(3) already exists.
# postgres=# insert into tangerine default values;
# ERROR:  duplicate key value violates unique constraint "tangerine_pkey"
# DETAIL:  Key (i)=(4) already exists.
# postgres=# insert into tangerine default values;
# INSERT 0 1