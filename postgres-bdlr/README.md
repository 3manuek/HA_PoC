create extension "uuid-ossp";
create extension postgres_fdw;
-- uuid_generate_v4() entirely from random nums
uuid_generate_v4()


CREATE TABLE tangerine (source uuid default uuid_generate_v4(), dia timestamp, payload text);

insert into tangerine (dia,payload) select uuid_generate_v4(), clock_timestamp(), random()::text from generate_series(1,1000);

CREATE PUBLICATION full_tangerine
FOR TABLE tangerine;

On pg2:

CREATE SERVER pg1 
  FOREIGN DATA WRAPPER postgres_fdw 
  OPTIONS (host 'postgres1', port '5432'); -- user mapping created later
CREATE USER MAPPING FOR postgres SERVER pg1 OPTIONS (user 'postgres');
IMPORT FOREIGN SCHEMA public LIMIT TO (tangerine) FROM SERVER pg1 INTO public;






function ppf {
for pp in  5 6
do
export DROP="DROP TABLE tangerine; "
export INZ="insert into tangerine (source,dia,payload) select random(), clock_timestamp(), random()::text from generate_series(1,10);"
export COUNT="select count(*) from tangerine"
export CREATE="${DROP} CREATE TABLE tangerine (sid id double precision PRIMARY KEY, dia timestamp, payload text); ${INZ} ; ${COUNT}"
export DROPS="DROP SUBSCRIPTION s_tangerine; "

psql -h localhost -p 1555${pp} -U postgres <<EOF
${1}
EOF
done
}


ppf "drop subscription s_tangerine;"

ppf $CREATE


psql -h localhost -p 15555 -U postgres <<EOF
DROP PUBLICATION full_tangerine;
CREATE PUBLICATION full_tangerine FOR TABLE tangerine;
EOF

psql -h localhost -p 15556 -U postgres <<EOF
DROP PUBLICATION append_tangerine;
CREATE PUBLICATION append_tangerine FOR TABLE tangerine WITH (publish = 'insert'); 
EOF

ppf $DROPS

psql -h localhost -p 15555 -U postgres <<EOF
CREATE SUBSCRIPTION s_tangerine
  CONNECTION 'host=postgres2 user=postgres' PUBLICATION append_tangerine WITH (copy_data =  true);
EOF

psql -h localhost -p 15556 -U postgres <<EOF
CREATE SUBSCRIPTION s_tangerine
  CONNECTION 'host=postgres1  user=postgres' PUBLICATION full_tangerine WITH (copy_data =  true);
EOF

