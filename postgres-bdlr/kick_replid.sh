#!/bin/bash

export DROP="DROP TABLE IF EXISTS tangerine;"
export INZ="insert into tangerine default values;"
export COUNT="select count(*) from tangerine"
export CREATE="
CREATE TABLE IF NOT EXISTS tangerine( \
    i bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,\
    f text default random()::text \
);"
export CHECK_VALUES="SELECT i,f FROM tangerine"



function ppf {

for pp in  5 6
do
echo $pp --
psql -q -h localhost -p 1555${pp} -U postgres <<EOF
${@}
EOF

done
}


echo "pubs"
psql -h localhost -p 15555 -U postgres <<EOF
DROP PUBLICATION full_tangerine;
EOF


ppf "drop subscription s_tangerine;"

echo "create"
ppf $DROP
ppf $CREATE

psql -h localhost -p 15555 -U postgres <<EOF
${INZ}
EOF


echo "pubs"
psql -h localhost -p 15555 -U postgres <<EOF
CREATE PUBLICATION full_tangerine FOR TABLE tangerine;
EOF


psql -h localhost -p 15556 -U postgres <<EOF
CREATE SUBSCRIPTION s_tangerine
  CONNECTION 'host=postgres1  user=postgres' PUBLICATION full_tangerine WITH (copy_data =  true, enabled = true);
EOF

sleep 0.5 
ppf $COUNT
ppf $CHECK_VALUES
psql -h localhost -p 15555 -U postgres <<EOF
${INZ}
EOF
psql -h localhost -p 15555 -U postgres <<EOF
${INZ}
EOF
psql -h localhost -p 15555 -U postgres <<EOF
${INZ}
EOF


# ppf $INZ
# ppf $COUNT
# ppf $INZ
# ppf $COUNT
# ppf $INZ
# ppf $COUNT
ppf $CHECK_VALUES

echo "let's see what destination does inserting def values on destination"
psql -h localhost -p 15556 -U postgres <<EOF
${INZ}
EOF



