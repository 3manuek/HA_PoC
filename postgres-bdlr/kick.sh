#!/bin/bash

export DROP="DROP TABLE IF EXISTS tangerine;"
export INZ="insert into tangerine (source,dia,payload) select md5(inet_server_addr()::text || uuid_generate_v4()::text ) ,  clock_timestamp(), random()::text from generate_series(1,10);"
export COUNT="select count(*) from tangerine"
export CREATE="CREATE TABLE tangerine (source text, dia timestamp, payload text, PRIMARY KEY (source) )  ; "


function ppf {

for pp in  5 6
do
echo $pp --
psql -h localhost -p 1555${pp} -U postgres <<EOF
${@}
EOF

done
}


echo "pubs"
psql -h localhost -p 15555 -U postgres <<EOF
DROP PUBLICATION full_tangerine;
EOF

psql -h localhost -p 15556 -U postgres <<EOF
DROP PUBLICATION append_tangerine;
EOF



ppf "drop subscription s_tangerine;"

echo "create"
ppf $DROP
ppf $CREATE

psql -h localhost -p 15555 -U postgres <<EOF
${INZ}
EOF


echo "pubs"
psql -h localhost -p 15555 -U postgres <<EOF
CREATE PUBLICATION full_tangerine FOR TABLE tangerine;
EOF

psql -h localhost -p 15556 -U postgres <<EOF
CREATE PUBLICATION append_tangerine FOR TABLE tangerine WITH (publish = 'insert');
EOF


psql -h localhost -p 15555 -U postgres <<EOF
CREATE SUBSCRIPTION s_tangerine
  CONNECTION 'host=postgres2 user=postgres' PUBLICATION append_tangerine WITH (copy_data = false, enabled = true);
EOF

psql -h localhost -p 15556 -U postgres <<EOF
CREATE SUBSCRIPTION s_tangerine
  CONNECTION 'host=postgres1  user=postgres' PUBLICATION full_tangerine WITH (copy_data =  true, enabled = true);
EOF


ppf $COUNT


ppf $INZ
ppf $COUNT
ppf $INZ
ppf $COUNT
ppf $INZ
ppf $COUNT



