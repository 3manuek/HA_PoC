#!/bin/bash

[ -z $(docker-compose ps -q )] && docker-compose up -d

docker-compose down

for pp in $(ls .data)
do
    echo -e "\n#added auto\ninclude = 'pglogical.conf'\n" >> .data/${pp}/postgresql.conf 


    cat > .data/${pp}/pglogical.conf <<_EOF
    wal_level=logical
    max_wal_senders=20
    max_replication_slots=20
    max_logical_replication_workers=20
    max_sync_workers_per_subscription=2
_EOF
done

docker-compose up -d