# vagrant_galera

Simple architecture of 3 nodes runing XtraDB. 
Extra documentation: https://palominodb.atlassian.net/wiki/display/PDBINT/MySQL+-+Galera


# Base OS

UPDATE:

Ol' box is not present anymore:
> vagrant box add tungsten_centos5_9 http://tag1consulting.com/files/centos-5.9-x86-64-minimal.box

Using the following now:

```
vagrant box add mizzy/centos-6.4 https://atlas.hashicorp.com/mizzy/boxes/centos-6.4
```

# Initial provisioning

Using:

>  vagrant up

or

>  vagrant --reload provision


# vagrant_galera
