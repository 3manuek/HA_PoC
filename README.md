# HA_PoC

Open Source ~~Relational~~ _SQL_ Databases HA and sharding concepts PoCs for testing purposes.

This repository collects a few laboratories about HA with consensus and clusterization.

- Consensus as the Entity Consistency improved warrantee over simplist approaches.
- Clusterization as the solution for _write scalability_.  



## Basic concepts for newcomers

Sharding is the technique to split relations between different endpoints without losing 
the super-relation consistency. That is, a relation must still consistent at tuple-level,
with no conflicts between nodes. This apply to reads either, on which we need generally 
at cluster level (here comes other advanced concepts like seriabilizity,  
snapshot isoaltion, tx certification, async replication and mixes). This is the isolation
between transactions and the consistency on commit on each node. That's why linearizability
and serializability do role in how each of the technologies work.


For more context:

[linearizability-versus-serializability](http://www.bailis.org/blog/linearizability-versus-serializability/)
[serializable snapshot isolation](https://arxiv.org/pdf/1208.4179.pdf) This is per node althouth,
not related to sharding.

Replica Factor: Number of nodes acting as replicas from a single-master cluster.
Masters are not counted as replicas, as they are assumed to not be in the transaction
commit quorum or neither accept writes.


## Chosen techs

- Cockroachdb
- PostgreSQL
- MySQL (InnoDB Cluster). ProxySQL used as GR entrypoints.

## Prerequisites 

- Docker
- Internet connectivity (not all the images are build).


## Other recommended projects:

- [PostDock](https://github.com/paunin/PostDock)

## Presentations

- The good, the ugly and the bad ([nerdear.la 2018](https://nerdear.la/) ) 